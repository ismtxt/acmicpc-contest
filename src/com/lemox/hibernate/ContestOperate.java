package com.lemox.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ContestOperate
{
	private Session session = null;
	
	public ContestOperate ()
	{
		Configuration config = new Configuration().configure();
		SessionFactory factory = config.buildSessionFactory();
		this.session = factory.openSession();
	}
	
	public void insert (Contest contest)
	{
		Transaction tran = this.session.beginTransaction();
		this.session.save(contest);
		tran.commit();
	}
	
	public Contest search (int contest_id)
	{
		Contest contest = null;
		
		ContestDAO contestDAO = new ContestDAO();
		contest = contestDAO.findById(4);
		System.out.println();
		return contest;
	}
	
}
