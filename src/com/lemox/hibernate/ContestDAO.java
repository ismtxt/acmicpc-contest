package com.lemox.hibernate;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Contest entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.lemox.hibernate.Contest
 * @author MyEclipse Persistence Tools
 */
public class ContestDAO extends BaseHibernateDAO
{
	private static final Logger log = LoggerFactory.getLogger(ContestDAO.class);
	// property constants
	public static final String CONTEST_ID = "contestId";
	public static final String OJ = "oj";
	public static final String LINK = "link";
	public static final String NAME = "name";
	public static final String WEEK = "week";
	public static final String ACCESS = "access";

	public void save(Contest transientInstance)
	{
		log.debug("saving Contest instance");
		try
		{
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re)
		{
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Contest persistentInstance)
	{
		log.debug("deleting Contest instance");
		try
		{
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re)
		{
			log.error("delete failed", re);
			throw re;
		}
	}

	public Contest findById(java.lang.Integer id)
	{
		log.debug("getting Contest instance with id: " + id);
		try
		{
			Contest instance = (Contest) getSession().get(
					"com.lemox.hibernate.Contest", id);
			return instance;
		} catch (RuntimeException re)
		{
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Contest instance)
	{
		log.debug("finding Contest instance by example");
		try
		{
			List results = getSession()
					.createCriteria("com.lemox.hibernate.Contest")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re)
		{
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value)
	{
		log.debug("finding Contest instance with property: " + propertyName
				+ ", value: " + value);
		try
		{
			String queryString = "from Contest as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re)
		{
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByContestId(Object contestId)
	{
		return findByProperty(CONTEST_ID, contestId);
	}

	public List findByOj(Object oj)
	{
		return findByProperty(OJ, oj);
	}

	public List findByLink(Object link)
	{
		return findByProperty(LINK, link);
	}

	public List findByName(Object name)
	{
		return findByProperty(NAME, name);
	}

	public List findByWeek(Object week)
	{
		return findByProperty(WEEK, week);
	}

	public List findByAccess(Object access)
	{
		return findByProperty(ACCESS, access);
	}

	public List findAll()
	{
		log.debug("finding all Contest instances");
		try
		{
			String queryString = "from Contest";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re)
		{
			log.error("find all failed", re);
			throw re;
		}
	}

	public Contest merge(Contest detachedInstance)
	{
		log.debug("merging Contest instance");
		try
		{
			Contest result = (Contest) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re)
		{
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Contest instance)
	{
		log.debug("attaching dirty Contest instance");
		try
		{
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re)
		{
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Contest instance)
	{
		log.debug("attaching clean Contest instance");
		try
		{
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re)
		{
			log.error("attach failed", re);
			throw re;
		}
	}

	/*
	 * 
	 * */
	public Contest getSingleContestByContestID(int id)
	{

		Configuration config = new Configuration().configure();
		SessionFactory factory = config.buildSessionFactory();
		Session session = factory.openSession();
		Transaction tran = session.beginTransaction();
		Contest contest = null;

		contest = (Contest) session.get(Contest.class, id);
		tran.commit();

		return contest;
	}

}