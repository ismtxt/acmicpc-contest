package com.lemox.hibernate;

import java.sql.Timestamp;

/**
 * Contest entity. @author MyEclipse Persistence Tools
 */

public class Contest implements java.io.Serializable
{

	// Fields

	private Integer id;
	private Integer contestId;
	private String oj;
	private String link;
	private String name;
	private Timestamp startTime;
	private String week;
	private String access;

	// Constructors

	/** default constructor */
	public Contest()
	{
	}

	/** minimal constructor */
	public Contest(Integer contestId)
	{
		this.contestId = contestId;
	}

	/** full constructor */
	public Contest(Integer contestId, String oj, String link, String name,
			Timestamp startTime, String week, String access)
	{
		this.contestId = contestId;
		this.oj = oj;
		this.link = link;
		this.name = name;
		this.startTime = startTime;
		this.week = week;
		this.access = access;
	}

	// Property accessors

	public Integer getId()
	{
		return this.id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getContestId()
	{
		return this.contestId;
	}

	public void setContestId(Integer contestId)
	{
		this.contestId = contestId;
	}

	public String getOj()
	{
		return this.oj;
	}

	public void setOj(String oj)
	{
		this.oj = oj;
	}

	public String getLink()
	{
		return this.link;
	}

	public void setLink(String link)
	{
		this.link = link;
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Timestamp getStartTime()
	{
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime)
	{
		this.startTime = startTime;
	}

	public String getWeek()
	{
		return this.week;
	}

	public void setWeek(String week)
	{
		this.week = week;
	}

	public String getAccess()
	{
		return this.access;
	}

	public void setAccess(String access)
	{
		this.access = access;
	}

}