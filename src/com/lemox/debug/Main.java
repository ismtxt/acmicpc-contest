package com.lemox.debug;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import com.lemox.hibernate.Contest;
import com.lemox.hibernate.ContestOperate;

public class Main
{

	public static String getJsonString(String url_path) throws Exception
	{
		URL url = new URL(url_path);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.connect();
		InputStream inputStream = connection.getInputStream();
		Reader reader = new InputStreamReader(inputStream, "UTF-8");
		BufferedReader bufferedReader = new BufferedReader(reader);
		String str = null;

		StringBuffer sb = new StringBuffer();

		while ((str = bufferedReader.readLine()) != null)
		{
			sb.append(str);
		}

		reader.close();
		connection.disconnect();

		return sb.toString();
	}
	
	public static void main(String[] args) throws Exception
	{
		// TODO Auto-generated method stub
		
		String result = null;
		result = getJsonString("http://contests.acmicpc.info/contests.json");
		
		JSONArray result_array = new JSONArray(result);
		
		
		for (int i = 0; i < result_array.length(); i++)
		{
			JSONObject contest_obj = result_array.optJSONObject(i);
			Contest contest = new Contest();
			contest.setContestId(Integer.valueOf(contest_obj.getInt("id")));
			contest.setOj(contest_obj.getString("oj"));
			contest.setLink(contest_obj.getString("link"));
			contest.setName(contest_obj.getString("name"));
			
			ContestOperate operate = new ContestOperate();
			operate.insert(contest);
		}
			
		
		/*Contest contest = new Contest();
		contest.setContestId(3327);
		contest.setOj("Codechef");
		ContestOperate operate = new ContestOperate();
		*/
		
		
		
		ContestOperate operate = new ContestOperate();
		operate.search(0);
		
	}

}
